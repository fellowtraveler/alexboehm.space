+++
title = "About"
description = "I'm Alex, a FOSS and self hosting advocate and a programming student at the 42 Adelaide school."
date = "2019-12-10"
aliases = ["about-me", "contact"]
author = "Alex Boehm"
+++

I am an enthusiastic programmer and student at the 42 Adelaide school
looking for part time work to utilise my skills and enable expansion.

I am passionate about technology, have years of experience in system
administration of software and hardware.
